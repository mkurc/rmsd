from setuptools import setup
from rmsd import __version__

with open('requirements.txt') as f:
    requirements = f.read().splitlines()

setup(
    name='rmsd',
    version=__version__,
    packages=['rmsd'],
    install_requires=requirements,
    url='https://bitbucket.org/mkurc/rmsd',
    license='MIT',
    author='mkurc',
    author_email='mkurc@chem.uw.edu.pl',
    description='Rmsd calculator for protein structures',
    entry_points={
        'console_scripts': ['rmsd=rmsd.__main__:main']
    }
)
