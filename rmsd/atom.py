import re
import os
import io
import requests
import gzip
import numpy as np
from collections import OrderedDict
from cached_property import cached_property
from copy import deepcopy

from rmsd.vector3d import Vector3d as v3d
from rmsd.selection import Selection


class InvalidPdbCode(Exception):

    """Exception raised when invalid pdb code is used."""

    msg = 'Invalid pdb code: {}'

    def __init__(self, code):
        self.code = code

    def __str__(self):
        return self.msg.format(self.code)


class PdbFile:

    """Class used to retrieve pdb files. Downloaded files are stored in cache for future use."""

    PDB_CACHE = os.path.join(os.path.expanduser('~'), '.PDBcache')
    RCSB_URL = 'http://files.rcsb.org/download/{}.pdb.gz'

    def __init__(self, code):
        self.code = code.lower()
        try:
            self.opened_file = gzip.open(self.filepath, 'rt')
        except FileNotFoundError:
            self.opened_file = self.fetch_from_db()
        if not self.opened_file:
            raise InvalidPdbCode(self.code)

    @cached_property
    def filepath(self):
        return os.path.join(self.PDB_CACHE, self.code[1:3], '{}.pdb.gz'.format(self.code))

    @cached_property
    def url(self):
        return self.RCSB_URL.format(self.code)

    def fetch_from_db(self):
        request = requests.get(self.url)
        if request.status_code == 200:
            os.makedirs(os.path.dirname(self.filepath), exist_ok=True)
            with open(self.filepath, 'wb') as f:
                f.write(request.content)
            return gzip.open(io.BytesIO(request.content), 'rt')

    def __enter__(self):
        return self.opened_file

    def __exit__(self, *args):
        self.opened_file.close()


class Atom:

    """Class representing single atom."""

    # Exception raised when trying to initialize Atom instance from invalid string
    class InvalidAtomString(Exception):
        pass

    # pattern for parsing ATOM record from the pdb file
    ATOM_PATT = re.compile('''^
        (?P<hetero>(ATOM[ ]{2}|HETATM)) # hetero
        (?P<serial>[0-9 ]{5})           # serial number
        (?P<name>[A-Z0-9 ]{5})          # name
        (?P<altloc>[A-Z ])              # alternative locator
        (?P<resname>[A-Z]{3})           # amino acid name
        (?P<chain>[ ][A-Z])             # chain id
        (?P<resnum>[0-9 ]{3}[0-9])      # residue number
        (?P<icode>[A-Z ])               # insertion code
        (?P<x>[-0-9 ]{7}[.][0-9]{3})    # x coordinate
        (?P<y>[-0-9 ]{4}[.][0-9]{3})    # y coordinate
        (?P<z>[-0-9 ]{4}[.][0-9]{3})    # z coordinate
        (?P<occ>[0-9 ]{3}[.][0-9]{2})   # occupancy
        (?P<bfac>[0-9 ]{3}[.][0-9]{2})  # beta factor
    ''', re.X)

    hetero = False
    serial = 0
    name = 'CA'
    altloc = ' '
    resname = 'UNK'
    chain = 'X'
    resnum = 0
    icode = ' '
    x = 0.0
    y = 0.0
    z = 0.0
    occ = 1.0
    bfac = 0.0
    model = None

    def __init__(self, **kwargs):
        for arg, val in kwargs.items():
            if hasattr(self, arg):
                attr = getattr(self, arg)
                if type(attr) is float:
                    val = float(val)
                elif type(attr) is int:
                    val = int(val)
                elif type(attr) is bool:
                    val = val == 'HETATM'
                elif type(attr) is str and val != ' ':
                    val = val.strip()
                setattr(self, arg, val)

    def __str__(self):
        name = self.name if len(self.name) == 4 else ' %-3s' % self.name
        return '{}{:5d} {}{}{} {}{:4d}{}   {:8.3f}{:8.3f}{:8.3f}{:6.2f}{:6.2f}'.format(
            'HETATM' if self.hetero else 'ATOM  ',
            self.serial,
            name,
            self.altloc,
            self.resname,
            self.chain,
            self.resnum,
            self.icode,
            self.x,
            self.y,
            self.z,
            self.occ,
            self.bfac
        )

    @cached_property
    def resid(self):
        return '{:d}{}'.format(self.resnum, self.icode).strip()

    @cached_property
    def hydro(self):
        return re.match('^(H.*|[0-9]*H.*)', self.name) is not None

    @property
    def R(self):
        return v3d(self.x, self.y, self.z)

    @R.setter
    def R(self, vector):
        self.x = vector.x
        self.y = vector.y
        self.z = vector.z

    @classmethod
    def from_string(cls, line):
        match = re.match(cls.ATOM_PATT, line)
        if match:
            return cls(**match.groupdict())
        else:
            raise cls.InvalidAtomString


# container for atoms
class Atoms:

    def __init__(self, atoms=()):
        self.atoms = [a for a in atoms]

    def __iter__(self):
        return iter(self.atoms)

    def __getitem__(self, key):
        if isinstance(key, slice):
            return Atoms(self.atoms[key])
        return self.atoms[key]

    def __setitem__(self, key, value):
        self.atoms[key] = value

    def __len__(self):
        return len(self.atoms)

    def __str__(self):
        return '\n'.join([str(atom) for atom in self])

    def index(self, item):
        return self.atoms.index(item)

    def append(self, atom):
        self.atoms.append(atom)

    def extend(self, other):
        self.atoms.extend([a for a in other])

    @classmethod
    def from_fileobject(cls, file):
        atoms = []
        current_model = None
        for line in file:
            if line.startswith('MODEL'):
                current_model = int(line.split()[1])
            elif line.startswith('ENDMDL'):
                current_model = None
            else:
                try:
                    atom = Atom.from_string(line)
                    atom.model = current_model
                    atoms.append(atom)
                except Atom.InvalidAtomString:
                    pass
        return cls(atoms)

    @classmethod
    def from_file(cls, filename):
        with open(filename) as f:
            return cls.from_fileobject(f)

    @classmethod
    def from_code(cls, pdb_code):
        with PdbFile(pdb_code) as f:
            return cls.from_fileobject(f)

    @cached_property
    def models(self):
        models = OrderedDict()
        for atom in self:
            key = atom.model
            if key not in models:
                models[key] = Atoms()
            models[key].append(atom)
        return models

    @cached_property
    def chains(self):
        chains = OrderedDict()
        for atom in self:
            key = atom.chain
            if key not in chains:
                chains[key] = Atoms()
            chains[key].append(atom)
        return chains

    @cached_property
    def residues(self):
        residues = OrderedDict()
        for atom in self:
            key = atom.resid
            if key not in residues:
                residues[key] = Atoms()
            residues[key].append(atom)
        return residues

    @cached_property
    def models_count(self):
        return len(self.models)

    @cached_property
    def chains_count(self):
        return len(self.chains)

    @cached_property
    def residues_count(self):
        return len(self.residues)

    @cached_property
    def models_list(self):
        return list(self.models.values())

    @cached_property
    def chains_list(self):
        return list(self.chains.values())

    @cached_property
    def residues_list(self):
        return list(self.residues.values())

    @property
    def pdb(self):
        pdb = ''
        for model, atoms in self.models.items():
            if model:
                pdb += 'MODEL{:9d}\n'.format(model) + str(atoms) + '\nENDMDL\n'
            else:
                pdb += str(atoms)
        return pdb

    @property
    def numpy(self):
        return np.concatenate([atom.R.numpy.reshape(1, 3) for atom in self])

    @numpy.setter
    def numpy(self, coords):
        for atom, coord in zip(self, coords):
            atom.R = v3d(coord)

    def move(self, vector):
        for atom in self:
            atom.R += vector
        return self

    @property
    def cent_of_mass(self):
        return v3d(np.average(self.numpy, 0))

    @cent_of_mass.setter
    def cent_of_mass(self, vector):
        self.move(vector - self.cent_of_mass)

    def center_at_origin(self):
        self.cent_of_mass = v3d()
        return self

    def rotate(self, matrix, is_centered=False):
        coord = self.numpy
        if not is_centered:
            com = np.average(coord, 0)
            coord = np.dot(coord - com, matrix) + com
        else:
            coord = np.dot(coord, matrix)
        self.numpy = coord
        return self

    def select(self, selection):
        if not isinstance(selection, Selection):
            selection = Selection(selection)
        return Atoms([atom for atom in self if selection.match(atom)])

    def drop(self, selection):
        if not isinstance(selection, Selection):
            selection = Selection(selection)
        return Atoms([atom for atom in self if not selection.match(atom)])


class Trajectory:

    """Class to hold multiple structures of the same collection of atoms"""

    def __init__(self, template, coordinates):
        self.template = template
        self.coordinates = coordinates

    @classmethod
    def from_atoms(cls, atoms):
        models = atoms.models_list
        template = models[0]
        coordinates = atoms.numpy.reshape(-1, len(template), 3)
        return cls(template, coordinates)

    def select(self, selection):
        template = self.template.select(selection)
        return Trajectory(template, self.coordinates[:, [self.template.index(a) for a in template], :])

    @property
    def atoms(self):
        atoms = Atoms()
        for i in range(self.coordinates.shape[0]):
            model = deepcopy(self.template)
            for atom in model:
                atom.model = i + 1
            model.numpy = self.coordinates[i]
            atoms.extend(model)
        return atoms

    @property
    def models(self):
        return self.coordinates.shape[0]


if __name__ == '__main__':
    a = Atoms.from_code('1rpr')
    t = Trajectory.from_atoms(a).select('name CA and resnum 1-10')
    b = t.atoms
    print(b.pdb)

