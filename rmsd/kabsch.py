import numpy as np


def kabsch(target, query, weights=None, concentric=False):
    """
    Function for the calculation of the best fit rotation.

    target     - a N x 3 np.array with coordinates of the reference structure
    query      - a N x 3 np.array with coordinates of the fitted structure
    weights    - a N-length list with weights - floats from [0:1]
    concentric - True/False specifying if target and query are centered at origin

    IMPORTANT: If weights are not None centering at origin should account for them.
    proper procedure: A -= np.average(A, 0, WEIGHTS)

    returns rotation matrix as 3 x 3 np.array
    """

    if not concentric:
        t = target - np.average(target, axis=0, weights=weights)
        q = query - np.average(query, axis=0, weights=weights)
    else:
        t = target
        q = query

    c = np.dot(weights * t.T, q) if weights else np.dot(t.T, q)
    v, s, w = np.linalg.svd(c)
    d = np.identity(3)
    if np.linalg.det(c) < 0:
        d[2, 2] = -1

    return np.dot(np.dot(w.T, d), v.T)


_LARGE = 1000.  # sort of ...
_TINY = 0.001   # useful only for rmsd/rmsf calc


def rmsd(target, query=None, weights=None):
    _diff = target if query is None else query - target
    _rmsd = np.sqrt(np.average(np.sum(_diff ** 2, axis=1), axis=0, weights=weights))
    return _rmsd if _rmsd > _TINY else 0.


GAUSS_MAX_ITER = 100


def dynamic_kabsch(target, query):
    _rmsd = _LARGE
    w = [1.0] * len(target)
    for i in range(GAUSS_MAX_ITER):
        t_com = np.average(target, 0, w)
        q_com = np.average(query, 0, w)
        t = target - t_com
        q = query - q_com
        r = kabsch(target=t, query=q, weights=w, concentric=True)
        q = np.dot(q, r)
        _diff = q - t
        _current = rmsd(_diff, weights=w)
        if np.abs(_current - _rmsd) < _TINY:
            return _rmsd, r, t_com, q_com
        _rmsd = _current
        w = np.exp(-np.sum(_diff ** 2, axis=1) / max(_rmsd, 2.)).tolist()
    else:
        raise Exception('Dynamic Kabsch did not converge in %i steps.' % GAUSS_MAX_ITER)
