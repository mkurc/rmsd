"""
Module for handling 3d vectors.
"""

import numpy as np
from random import uniform
from math import sin, cos, sqrt, pi


class Vector3d:
    """
    Vector3d is a class to handle 3d vectors of floats.
    The coordinates are accessible by attributes x, y, z.
    """

    def __init__(self, *args, **kwargs):

        self.x = self.y = self.z = 0.0

        if args:
            if len(args) == 3:
                self.x = float(args[0])
                self.y = float(args[1])
                self.z = float(args[2])

            elif len(args) == 1:
                arg = args[0]

                if isinstance(arg, str):
                    words = arg.replace(',', ' ').split()

                    if len(words) == 3:
                        self.x = float(words[0])
                        self.y = float(words[1])
                        self.z = float(words[2])
                    else:
                        raise Exception('Invalid string used to initialize Vector3d: "%s"' % arg)
                else:
                    try:
                        self.x = arg.x
                        self.y = arg.y
                        self.z = arg.z
                    except AttributeError:
                        self.x = arg[0]
                        self.y = arg[1]
                        self.z = arg[2]
            else:
                raise Exception('Vector3d takes either 1 or 3 arguments instead of %d given' % len(args))

        if kwargs:
            for key, val in kwargs.items():
                if key in ['x', 'y', 'z']:
                    self.__setattr__(key, float(val))
                else:
                    raise Exception('Unknown keyword argument: %s=%s used to initialize Vector3d' % (key, val))

    def __str__(self):
        return "%8.3f%8.3f%8.3f" % (self.x, self.y, self.z)

    def __repr__(self):
        return str(self)

    def __add__(self, other):
        return Vector3d(self.x + other.x, self.y + other.y, self.z + other.z)

    def __sub__(self, other):
        return Vector3d(self.x - other.x, self.y - other.y, self.z - other.z)

    def __pos__(self):
        return Vector3d(self.x, self.y, self.z)

    def __neg__(self):
        return Vector3d(-self.x, -self.y, -self.z)

    def __mul__(self, factor):
        return Vector3d(self.x * factor, self.y * factor, self.z * factor)

    def __rmul__(self, factor):
        return self * factor

    def __div__(self, factor):
        return self * (1.0 / factor)

    def dot(self, other):
        """
        Dot product of two vectors.
        :return: float
        """
        return self.x * other.x + self.y * other.y + self.z * other.z

    def cross(self, other):
        """
        Cross product of two vectors.
        :return: Vector3d
        """
        return Vector3d(
            self.y * other.z - self.z * other.y,
            self.z * other.x - self.x * other.z,
            self.x * other.y - self.y * other.x
        )

    @property
    def mod2(self):
        """
        |vector|^2
        :return: float
        """
        return self.dot(self)

    @property
    def length(self):
        """
        Returns vector's length.
        :return: float
        """
        return self.mod2 ** 0.5

    @property
    def norm(self):
        """
        Returns normalized vector.
        :return: Vector3d
        """
        return self.__div__(self.length)

    def __iadd__(self, other):
        """
        Addition assignment.
        :param other: Vector3d 
        :return: Vector3d
        """
        self.x += other.x
        self.y += other.y
        self.z += other.z
        return self

    def __isub__(self, other):
        """
        Subtraction assignment.
        :param other: Vector3d 
        :return: Vector3d
        """
        self.x -= other.x
        self.y -= other.y
        self.z -= other.z
        return self

    def __imul__(self, factor):
        """
        Scalar multiplication assignment.
        :param factor: Vector3d 
        :return: Vector3d
        """
        self.x *= factor
        self.y *= factor
        self.z *= factor
        return self

    def __idiv__(self, factor):
        """
        Scalar division assignment.
        :param factor: Vector3d 
        :return: Vector3d
        """
        self.x /= factor
        self.y /= factor
        self.z /= factor
        return self

    @property
    def numpy(self):
        """
        Conversion to numpy
        """
        return np.array([self.x, self.y, self.z])

    def random(self):
        """
        Returns random normalized vector from spherical uniform distribution
        :return: Vector3d
        """
        phi = uniform(0., 2. * pi)
        cos_theta = uniform(-1., 1.)
        sin_theta = sqrt(1. - cos_theta ** 2)
        self.x = sin_theta * cos(phi)
        self.y = sin_theta * sin(phi)
        self.z = cos_theta
        return self
