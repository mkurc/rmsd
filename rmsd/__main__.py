from rmsd.atom import Atoms, Trajectory, InvalidPdbCode
from rmsd.selection import SYNTAX
from rmsd import kabsch, __version__
import argparse
import logging
import sys

# set numpy print options
kabsch.np.set_printoptions(precision=3, floatmode='fixed')


# exception raised when compared structures have different lengths
class LengthMismatchError(Exception):

    def __init__(self, len1, len2):
        self.msg = 'Compared structures have different lengths: {:d}(reference) vs {:d}(model)'.format(len1, len2)

    def __str__(self):
        return self.msg


class Rmsd:

    input = None             # input structure filename
    input_fit = None         # input selection string for fitting
    input_calc = None        # input selection string for rmsd calculation
    reference = None         # reference structure filename or pdb code
    reference_fit = None     # reference selection string for fitting
    reference_calc = None    # reference selection string for rmsd calculation
    output = None            # output filename
    save = None              # save filename

    def __init__(self, **kwargs):
        logging.debug(msg='Initializing Rmsd class')
        for key, val in kwargs.items():
            if hasattr(self, key):
                setattr(self, key, val)
                logging.debug(msg='Setting {} = {}'.format(key, val))
            else:
                raise AttributeError('{} is not a valid attribute for Rmsd class initialization.'.format(key))

        self.trajectory = None
        self.rmsd = []

    def run(self):

        # get input trajectory
        self.trajectory = self.get_input()

        # get reference
        reference = self.get_reference()

        # select input subset for fitting
        input_fit = self.select(self.trajectory, ('input', 'fit'))

        # select reference subset for fitting
        reference_fit = self.select(reference, ('reference', 'fit'))

        # fitting
        self.fitting(input_fit, reference_fit)

        # select input subset for rmsd calculation
        input_calc = self.select(self.trajectory, ('input', 'calc'))

        # select reference subset for rmsd calculation
        reference_calc = self.select(reference, ('reference', 'calc'))

        # calculate rmsd
        self.rmsd_calc(input_calc, reference_calc)

        # save superposed structures
        self.save_structures()

        # print output
        self.print_output()

    def get_input(self):
        logging.info(msg='Loading input structure(s) from "{}"'.format(self.input))
        input = Trajectory.from_atoms(Atoms.from_file(self.input))
        logging.debug('Loaded {:d} models from {}'.format(input.models, self.input))
        return input

    def get_reference(self):
        logging.info(msg='Loading reference structure from "{}"'.format(self.reference))
        try:
            models = Atoms.from_file(self.reference).models_list
            logging.debug('Reference structure loaded from file')
        except FileNotFoundError as e1:
            try:
                models = Atoms.from_code(self.reference).models_list
                logging.debug('Reference structure downloaded from the PDB database')
            except InvalidPdbCode as e2:
                raise e2 if len(self.reference) == 4 else e1
        logging.debug('Using model 1 of {} from {} as reference'.format(len(models), self.reference))
        return models[0]

    def select(self, structure, attrs):

        key = '_'.join(attrs)
        value = getattr(self, key)

        if value:
            substructure = structure.select(value)
            selection = value
        else:
            substructure = structure
            selection = 'all'

        logging.info('Selecting "{}" from {} structure for {}'.format(
            selection,
            attrs[0],
            'fitting' if attrs[1] == 'fit' else 'rmsd calculation'
        ))

        logging.debug('{:d} atoms selected'.format(
            len(substructure) if isinstance(substructure, Atoms) else len(substructure.template)
        ))

        return substructure

    def fitting(self, trajectory, reference):

        # get reference coordinates and move them to the center of origin
        target = reference.numpy
        target_com = kabsch.np.average(target, axis=0)
        target = target - target_com
        logging.debug(msg='Reference center of mass={}'.format(target_com))

        # check for size compatibility
        if target.shape[0] != trajectory.coordinates.shape[1]:
            raise LengthMismatchError(target.shape[0], trajectory.coordinates.shape[1])

        logging.debug(msg='Reference length: {:d} atoms, Input length: {:d} atoms'.format(
            target.shape[0],
            trajectory.coordinates.shape[1]
        ))

        # fitting
        logging.info(msg='Fitting input struture(s) to the reference structure')

        # iterate over all models
        for i in range(trajectory.models):

            # move model to cent of mass
            query = trajectory.coordinates[i]
            query_com = kabsch.np.average(query, axis=0)
            query = query - query_com

            # compute rotation
            rotation = kabsch.kabsch(target, query, concentric=True)

            # rotate and shift
            q = self.trajectory.coordinates[i]
            q = q - query_com
            q = kabsch.np.dot(q, rotation)
            self.trajectory.coordinates[i] = q + target_com

            logging.debug(
                'Model: {:d} shift: {} rotation: {}'.format(i + 1, target_com - query_com, rotation.reshape(1, -1))
            )

    def rmsd_calc(self, trajectory, reference):

        # get reference coordinates
        target = reference.numpy

        # check for size compatibility
        if target.shape[0] != trajectory.coordinates.shape[1]:
            raise LengthMismatchError(target.shape[0], trajectory.coordinates.shape[1])

        logging.debug(msg='Reference length: {:d} atoms, Input length: {:d} atoms'.format(
            target.shape[0],
            trajectory.coordinates.shape[1]
        ))

        # calculate rmsd
        logging.info(msg='Calculating rmsd')

        # iterate over all models
        for i in range(trajectory.models):

            # compute rmsd
            result = kabsch.rmsd(target, trajectory.coordinates[i])

            # append result to self.rmsd
            self.rmsd.append(result)

            logging.debug(msg='Model: {:d} Rmsd: {:.3f}'.format(i + 1, result))

    def format_output(self):

        # if multiple structures in input
        if len(self.rmsd) > 1:
            output = ['{}:{} {:.3f}'.format(self.input, model, rmsd) for model, rmsd in enumerate(self.rmsd, 1)]

        # if one structure in input
        else:
            output = ['{} {:.3f}'.format(self.input, self.rmsd[0])]

        return '\n'.join(output)

    def save_structures(self):
        if self.save:
            with open(self.save, 'w') as f:
                f.write(self.trajectory.atoms.pdb)
            logging.info(msg='Saving superposed structures to "{}"'.format(self.output))

    def print_output(self):
        output = self.format_output()

        # print to file
        if self.output:
            logging.info(msg='Saving output to {}'.format(self.output))
            with open(self.output, 'w') as f:
                f.write(output)

        # or print to stdout
        else:
            logging.debug(msg='Printing output')
            print(output)


EXAMPLES = '''
EXAMPLES:

1. Load "mymodel.pdb" and calculate rmsd to 1xyz pdb structure

\trmsd -i mymodel.pdb -r 1xyz

2. Load "mytrajectory.pdb" and calculate rmsd to the "reference.pdb" structure,
   save superposed trajectory to "superposed.pdb" 

\trmsd -i mytrajectory.pdb -r reference.pdb -o superposed.pdb 

3. Load mymodel_AB.pdb, superpose chain A of the mymodel_AB.pdb to the chain L of the 1xyz,
   calculate rmsd between chain B of the mymodel_AB.pdb and the chain H of the 1xyz. 

\trmsd -i mymodel_AB.pdb -if "chain A" -ic "chain B" -r 1xyz -rf "chain L" -rc "chain H"
'''


INTRO = '''
Program reads the pdb file with one or more structures (option: -i) and the reference structure (-r). If multiple models
are present in the input file - all are processed. If there are multiple models in the reference file, only the first
one is used.
'''


def main():

    parser = argparse.ArgumentParser(
        prog='rmsd',
        description='Rmsd calculator for protein structures',
        formatter_class=argparse.RawTextHelpFormatter,
        add_help=False,
    )

    input_group = parser.add_argument_group('Input structure', 'Options controlling how input structure is processed')
    reference_group = parser.add_argument_group('Reference structure', 'Options regarding the reference structure')
    output_group = parser.add_argument_group('Output options')
    misc_group = parser.add_argument_group('Miscellaneous options')
    log_group = misc_group.add_mutually_exclusive_group()
    help_group = misc_group.add_mutually_exclusive_group()

    input_group.add_argument(
        '-i', '--input',
        metavar='FILE',
        required=True,
        help='load pdb FILE with protein structure(s)'
    )

    input_group.add_argument(
        '-if', '--input-fit',
        metavar='SELECTION',
        default='name CA',
        help='selects which atoms from the input structure should be used for fitting (default="%(default)s")'
    )

    input_group.add_argument(
        '-ic', '--input-calc',
        metavar='SELECTION',
        default='name CA',
        help='selects which atoms from the input structure should be used for rmsd calculation (default="%(default)s")'
    )

    reference_group.add_argument(
        '-r', '--reference',
        metavar='FILE/CODE',
        required=True,
        help='load reference structure from pdb FILE or CODE'
    )

    reference_group.add_argument(
        '-rf', '--reference-fit',
        metavar='SELECTION',
        default='name CA',
        help='selects which atoms from the reference structure should be used for fitting (default="%(default)s")'
    )

    reference_group.add_argument(
        '-rc', '--reference-calc',
        metavar='SELECTION',
        default='name CA',
        help='selects which atoms from the reference structure should be used for rmsd calculation '
             + '(default="%(default)s")'
    )

    output_group.add_argument(
        '-o', '--output',
        metavar='FILE',
        help='print results to FILE'
    )

    output_group.add_argument(
        '-s', '--save',
        metavar='FILE',
        help='save superposed structures to FILE'
    )

    log_group.add_argument(
        '-q', '--quiet',
        action='store_true',
        help='set logging level to ERROR (default is INFO)'
    )

    log_group.add_argument(
        '-d', '--debug',
        action='store_true',
        help='set logging level to DEBUG (default is INFO)'
    )

    misc_group.add_argument(
        '-l', '--log',
        metavar='FILE',
        help='log to FILE'
    )

    misc_group.add_argument(
        '-v', '--version',
        action='store_true',
        help='print version and exit'
    )

    help_group.add_argument(
        '-h',
        action='store_true',
        help='print help message and exit'
    )

    help_group.add_argument(
        '--help',
        action='store_true',
        help='print full help message and exit'
    )

    # pre-parsing -v, -h, --help
    pre_parser = argparse.ArgumentParser(add_help=False)
    help_group = pre_parser.add_mutually_exclusive_group()
    help_group.add_argument('-h', action='store_true')
    help_group.add_argument('--help', action='store_true')
    pre_parser.add_argument('-v', '--version', action='store_true')
    pre_args, remains = pre_parser.parse_known_args()

    if pre_args.version:
        print('rmsd-{}'.format(__version__))
        sys.exit(0)
    elif pre_args.h:
        print(parser.format_help())
        sys.exit(0)
    elif pre_args.help:
        parser.description += INTRO
        separator = '-' * 100
        full_help = [parser.format_help(), separator, SYNTAX, separator, EXAMPLES]
        print('\n'.join(full_help))
        sys.exit(0)

    # parse args
    kwargs = vars(parser.parse_args(remains))

    # set verbosity
    if kwargs.pop('quiet', None):
        level = 'ERROR'
    elif kwargs.pop('debug', None):
        level = 'DEBUG'
    else:
        level = 'INFO'

    # log to file?
    logfile = kwargs.pop('log', None)

    logging.basicConfig(
        format='%(levelname)s:%(message)s',
        level=level,
        filename=logfile
    )

    # delete pre-parser args from kwargs
    for key in ['h', 'help', 'version']:
        del(kwargs[key])

    # run the program
    try:
        app = Rmsd(**kwargs)
        app.run()
    except Exception as e:
        logging.error(e)


if __name__ == '__main__':
    main()
