import unittest
from rmsd.__main__ import Rmsd


class BarnaseTest(unittest.TestCase):

    def test_rmsd_chainB(self):
        r = Rmsd(
            input='traj.pdb',
            input_fit='chain A',
            input_calc='chain B and not resnum 64',
            reference='2za4',
            reference_fit='name CA and chain A',
            reference_calc='name CA and chain B',
            output='/dev/null'
        )
        r.run()
        with open('res.txt') as f:
            results = [float(line) for line in f]

        diff = sum([(a - b) ** 2 for a, b in zip(results, r.rmsd)])

        self.assertLess(diff, 0.001)


if __name__ == '__main__':
    unittest.main()
