# Rmsd calculator for protein structures

#### Program reads the pdb file with one or more structures (option: -i) and the reference structure (-r). If multiple models are present in the input file - all are processed. If there are multiple models in the reference file, only the first one is used.

## Instalation

1. Download `rmsd-version.tar.gz` from the Downloads page
2. Run command: `python3 -m pip install rmsd-version.tar.gz`

---

```bash
usage: rmsd -i FILE [-if SELECTION] [-ic SELECTION] -r FILE/CODE
            [-rf SELECTION] [-rc SELECTION] [-o FILE] [-s FILE] [-q | -d]
            [-l FILE] [-v] [-h | --help]
            
Input structure:
  Options controlling how input structure is processed

  -i FILE, --input FILE
                        load pdb FILE with protein structure(s)
  -if SELECTION, --input-fit SELECTION
                        selects which atoms from the input structure should be used for fitting (default="name CA")
  -ic SELECTION, --input-calc SELECTION
                        selects which atoms from the input structure should be used for rmsd calculation (default="name CA")

Reference structure:
  Options regarding the reference structure

  -r FILE/CODE, --reference FILE/CODE
                        load reference structure from pdb FILE or CODE
  -rf SELECTION, --reference-fit SELECTION
                        selects which atoms from the reference structure should be used for fitting (default="name CA")
  -rc SELECTION, --reference-calc SELECTION
                        selects which atoms from the reference structure should be used for rmsd calculation (default="name CA")

Output options:
  -o FILE, --output FILE
                        print results to FILE
  -s FILE, --save FILE  save superposed structures to FILE

Miscellaneous options:
  -q, --quiet           set logging level to ERROR (default is INFO)
  -d, --debug           set logging level to DEBUG (default is INFO)
  -l FILE, --log FILE   log to FILE
  -v, --version         print version and exit
  -h                    print help message and exit
  --help                print full help message and exit

```

## SELECTION syntax:

* Selection sentence is a combination of keywords and operators.
* Keywords may be followed by argument(s).
* Keywords and operators are case-insensitive, while arguments aren't.
* If used in the command line, selection sentence must be enclosed in "".

### I. Keywords

#### 1. No argument keywords:
* __HETERO__ selects non-protein atoms
* __HYDRO__ selects hydrogen atoms

#### 2. Keywords with character arguments:
* __CHAIN__ selects by chain id
* __ALTLOC__ selects by alternative locations
* __ICODE__ selects by insertion codes

examples: `altloc A` `chain A, B, C`

#### 3. Keywords with string arguments:
* __NAME__ selects by atom name
* __RESNAME__ selects by residue name
* __RESID__ selects by residue id (number + insertion code)

examples: `name CA` `resname VAL, ILE` `resid 123, 123A`
            
#### 4. Keywords with integer arguments:
* __MODEL__ selects by model number
* __RESNUM__ selects by residue number
* __SERIAL__ selects by serial number

examples: `model 1` `resnum 25-33`
            
#### 5. Keywords with float arguments:
* __BFAC__ selects by beta factor
* __OCC__ selects by occupancy

examples: `bfac 0.1-0.5, 0.75-0.9` `occ 1.0`

### II. Operators

* __,__ separates multiple arguments for one keyword i.e. `name N, CA, C, O` selects all atom with names N, CA, C and O
* __+__ alias for __,__ i.e. `name N+CA+C+O` selects all atom with names N, CA, C and O
* __-__ range operator i.e. `resnum 25-33` selects all atoms in residues with numbers from 25 to 33; works also with character args i.e. `chain A-D` is equivalent to `chain A+B+C+D`
* __NOT__ negation logical operator, negates following keyword i.e. "not chain A" selects all chains except for chain A
* __AND__ logical conjunction i.e. "chain A and name CA" selects all CA atoms in chain A
* __OR__ logical alternation i.e. "chain A or chain B" selects all atoms in chain A or chain B
* __()__ parenthesis can be used to group expressions to be evaluated together

### III. Aliases

Aliases are shortcuts for frequent selections.

* `BACKBONE` == `PROTEIN and name N,CA,C,O`
* `SIDECHAIN` == `PROTEIN and not name N,CA,C,O`
* `HEAVY` == `not HYDRO`
* `HETATM` == `HETERO`
* `WATER` == `resname HOH`
* `PROTEIN` == `resname ALA,CYS,ASP,GLU,PHE,GLY,HIS,ILE,LYS,LEU,MET,ASN,PRO,GLN,ARG,SER,THR,VAL,TRP,TYR`

## EXAMPLES

Load "mymodel.pdb" and calculate rmsd to 1xyz pdb structure:

```rmsd -i mymodel.pdb -r 1xyz```

Load "mytrajectory.pdb" and calculate rmsd to the "reference.pdb" structure, save superposed trajectory to "superposed.pdb":

```rmsd -i mytrajectory.pdb -r reference.pdb -o superposed.pdb```

Load mymodel_AB.pdb, superpose chain A of the mymodel_AB.pdb to the chain L of the 1xyz, calculate rmsd between chain B of the mymodel_AB.pdb and the chain H of the 1xyz:

```rmsd -i mymodel_AB.pdb -if "chain A" -ic "chain B" -r 1xyz -rf "chain L" -rc "chain H"```
